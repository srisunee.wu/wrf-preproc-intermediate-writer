{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Copyright 2020 Jupiter Intelligence, Inc.\n",
    "\n",
    "   Licensed under the Apache License, Version 2.0 (the \"License\");\n",
    "   you may not use this file except in compliance with the License.\n",
    "   You may obtain a copy of the License at\n",
    "\n",
    "       http://www.apache.org/licenses/LICENSE-2.0\n",
    "\n",
    "   Unless required by applicable law or agreed to in writing, software\n",
    "   distributed under the License is distributed on an \"AS IS\" BASIS,\n",
    "   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.\n",
    "   See the License for the specific language governing permissions and\n",
    "   limitations under the License."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Example netCDF to WRF Intermediate preprocessing\n",
    "\n",
    "Luke Madaus -- Jupiter Intelligence -- 28 April 2020\n",
    "\n",
    "This notebook walks through an example of using the WRFInputFormatter object to convert netCDF output to WRF intermediate format output.  It works on some example netCDF files that were obtained from a 3-day period in the NOAA-CIRES 20th Century Reanalysis version 2 output, which should be included in the \"testdata\" subfolder of this repository.\n",
    "\n",
    "Example data source:\n",
    "```\n",
    "Compo, G.P., J.S. Whitaker, P.D. Sardeshmukh, N. Matsui, R.J. Allan, X. Yin, B.E. Gleason, R.S. Vose, G. Rutledge, P. Bessemoulin, S. Brönnimann, M. Brunet, R.I. Crouthamel, A.N. Grant, P.Y. Groisman, P.D. Jones, M. Kruk, A.C. Kruger, G.J. Marshall, M. Maugeri, H.Y. Mok, Ø. Nordli, T.F. Ross, R.M. Trigo, X.L. Wang, S.D. Woodruff, and S.J. Worley, 2011: The Twentieth Century Reanalysis Project. Quarterly J. Roy. Meteorol. Soc., 137, 1-28. DOI: 10.1002/qj.776 Free and Open Access.\n",
    "```\n",
    "\n",
    "Required python libraries are listed in the requirements.txt file.\n",
    "\n",
    "### Basic imports"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "from datetime import datetime\n",
    "import pandas as pd\n",
    "from wps_formatter import WRFInputFormatter"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Set paths to variables\n",
    "Different datasets will aggregate their atmospheric output in different ways.  Some will have separate netCDF files for each variable, others will have separate files for different years or dates.  The WRFInputFormatter object does have some flexibility for determining how to aggregate different files, but this example assumes that all the times you need from each variable are in the same file (though not that each variable is its own file).  \n",
    "\n",
    "We build a dictionary below pointing each variable to the file that contains it.  The naming conventions followed here loosely follow the CMOR standards for CMIP output, but a variety of variable naming conventions are recognized.  To use different variable names, check the\n",
    "`grib_codes.df.pkl` Dataframe, which is a list of all variable names and associated metadata that are recognized by WRFInputFormatter.  You can add additional lines to supply alternative variable names. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/html": [
       "<div>\n",
       "<style scoped>\n",
       "    .dataframe tbody tr th:only-of-type {\n",
       "        vertical-align: middle;\n",
       "    }\n",
       "\n",
       "    .dataframe tbody tr th {\n",
       "        vertical-align: top;\n",
       "    }\n",
       "\n",
       "    .dataframe thead th {\n",
       "        text-align: right;\n",
       "    }\n",
       "</style>\n",
       "<table border=\"1\" class=\"dataframe\">\n",
       "  <thead>\n",
       "    <tr style=\"text-align: right;\">\n",
       "      <th></th>\n",
       "      <th>Description</th>\n",
       "      <th>Grib Code</th>\n",
       "      <th>Level Code</th>\n",
       "      <th>Metgrid Name</th>\n",
       "      <th>Metgrid Units</th>\n",
       "    </tr>\n",
       "  </thead>\n",
       "  <tbody>\n",
       "    <tr>\n",
       "      <th>hus</th>\n",
       "      <td>Specific Humidity</td>\n",
       "      <td>52</td>\n",
       "      <td>100</td>\n",
       "      <td>SPECHUMD</td>\n",
       "      <td>kg kg-1</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>huss</th>\n",
       "      <td>Specific Humidity at 2 m</td>\n",
       "      <td>52</td>\n",
       "      <td>105</td>\n",
       "      <td>SPECHUMD</td>\n",
       "      <td>kg kg-1</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>lsmask</th>\n",
       "      <td>Land/Sea flag</td>\n",
       "      <td>81</td>\n",
       "      <td>1</td>\n",
       "      <td>LANDSEA</td>\n",
       "      <td></td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>mrlsl</th>\n",
       "      <td>Soil Moist</td>\n",
       "      <td>144</td>\n",
       "      <td>112</td>\n",
       "      <td>SM{:03d}{:03d}</td>\n",
       "      <td>fraction</td>\n",
       "    </tr>\n",
       "    <tr>\n",
       "      <th>ps</th>\n",
       "      <td>Surface Pressure</td>\n",
       "      <td>1</td>\n",
       "      <td>1</td>\n",
       "      <td>PSFC</td>\n",
       "      <td>Pa</td>\n",
       "    </tr>\n",
       "  </tbody>\n",
       "</table>\n",
       "</div>"
      ],
      "text/plain": [
       "                     Description  Grib Code  Level Code    Metgrid Name  \\\n",
       "hus            Specific Humidity         52         100        SPECHUMD   \n",
       "huss    Specific Humidity at 2 m         52         105        SPECHUMD   \n",
       "lsmask             Land/Sea flag         81           1         LANDSEA   \n",
       "mrlsl                 Soil Moist        144         112  SM{:03d}{:03d}   \n",
       "ps              Surface Pressure          1           1            PSFC   \n",
       "\n",
       "       Metgrid Units  \n",
       "hus          kg kg-1  \n",
       "huss         kg kg-1  \n",
       "lsmask                \n",
       "mrlsl       fraction  \n",
       "ps                Pa  "
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# These paths point to which netcdf file contains the each variable\n",
    "variable_paths = {\n",
    "    # Upper air variables\n",
    "    'ta' : './testdata/sample_reanalysis_upa.nc',\n",
    "    'hus' : './testdata/sample_reanalysis_upa.nc',\n",
    "    'ua' : './testdata/sample_reanalysis_upa.nc',\n",
    "    'va' : './testdata/sample_reanalysis_upa.nc',\n",
    "    \n",
    "    # Surface variables\n",
    "    'psl' : './testdata/sample_reanalysis_sfc.nc',\n",
    "    'ps' : './testdata/sample_reanalysis_sfc.nc',\n",
    "    'tas' : './testdata/sample_reanalysis_sfc.nc',\n",
    "    \n",
    "    # Soil variables\n",
    "    'soilw' : './testdata/sample_reanalysis_subsfc.nc',\n",
    "    'tsoil' : './testdata/sample_reanalysis_subsfc.nc',\n",
    "\n",
    "}\n",
    "\n",
    "# This table links variable names in the original file with their equivalent\n",
    "# names in WPS format\n",
    "grib_codes = pd.read_pickle('./testdata/grib_codes.df.pkl')\n",
    "grib_codes.head()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Build input parameters to WRFInputFormatter\n",
    "We build a dictionary of needed parameters to process this data.  There are a lot more options than these available in WRFInputFormatter, with various degrees of hardening to different data formats and structures.  But these are the basic requirements."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Start by supplying the variable_paths and the grib_codes table\n",
    "params = {\n",
    "    'variable_paths' : variable_paths,\n",
    "    'variable_table' : grib_codes,\n",
    "    \n",
    "    # Subset to this time range (start, end)\n",
    "    'time_range' : (datetime(2000,1,1,0), datetime(2000,1,2,12)),\n",
    "    \n",
    "    # We want output every this many hours.  This particular\n",
    "    # string format is what xarray looks for\n",
    "    'time_freq' : '6H',\n",
    "    \n",
    "    # Set the vertical coordinate type of the raw dataset.\n",
    "    # Options are 'pressure', with limited support for 'hybrid-p' and 'hybrid-z'\n",
    "    'vcoord_type' : 'pressure',\n",
    "    \n",
    "    # These are the names of the vertical coordinates in the datasets\n",
    "    # For upper air pressure levels:\n",
    "    'vcoord' : 'level',\n",
    "    \n",
    "    # And for subsurface soil levels\n",
    "    'soilcoord' : 'depth',\n",
    "    \n",
    "    # Optional...set a geographical subset bounds in lat/lon\n",
    "    # Lon in 0-365 space\n",
    "    # If not provided, will return the entire extend of the netCDF file dataset\n",
    "    'geog_bounds' : {'south': 22, 'north': 52, 'west': 360-135, 'east': 360-55},\n",
    "\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Initialize the WRFInputFormatter object\n",
    "\n",
    "We'll initialize the object with these parameters.  This will need to access at least two variables -- surface/sea-level pressure and upper-air temperature -- to figure out what the grid extents are going to be in both the horizontal and the vertical.  It allocates a pressure field in memory of the full size of the upper-air field, so can take up a fair amount of memory for large or long-period datasets.  The logger will print out some relevant information."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "2020-06-30 12:18:49,102 - WRFInputFormatter - INFO - THESE ARE GEOG BOUNDS: {'south': 22, 'north': 52, 'west': 225, 'east': 305}\n",
      "2020-06-30 12:18:49,110 - WRFInputFormatter - INFO - Building domain structure from pressure field\n",
      "2020-06-30 12:18:49,123 - WRFInputFormatter - INFO - Loading and subsetting ta\n",
      "2020-06-30 12:18:49,138 - WRFInputFormatter - INFO - Loading variable ta from: ./testdata/sample_reanalysis_upa.nc\n",
      "2020-06-30 12:18:49,417 - WRFInputFormatter - INFO - Data already at requested frequency of 6H\n",
      "2020-06-30 12:18:49,419 - WRFInputFormatter - WARNING - Suspect we have hPa...converting to Pa\n",
      "2020-06-30 12:18:49,437 - WRFInputFormatter - INFO - Done with pressure\n",
      "2020-06-30 12:18:49,439 - WRFInputFormatter - INFO - GEOG BOUNDS REQD: {'south': 22, 'north': 52, 'west': 225, 'east': 305}\n",
      "2020-06-30 12:18:49,440 - WRFInputFormatter - INFO - ACTUAL LATS: 20.0 -- 56.0\n",
      "2020-06-30 12:18:49,441 - WRFInputFormatter - INFO - ACTUAL LONS: 220.0 -- 306.0\n",
      "2020-06-30 12:18:49,451 - WRFInputFormatter - INFO - Done with initialization\n"
     ]
    }
   ],
   "source": [
    "wrfgen = WRFInputFormatter(**params)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Initialize the WRF intermediate files\n",
    "This method will create empty WRF intermediate files for each output time requested, and hold open pointers to these files in memory so they can be written to."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "wrfgen.open_WPS_files()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Populate the files with variable data\n",
    "This is the workhorse part of the process, consisting of 2-3 phases for each variable.  We loop through each of our variables and do two things:\n",
    "\n",
    "* The `load_and_subset(var)` method will open the netCDF file associated with the requested variable and subset it in space and time to our domain\n",
    "* If this is a soil variable, some additional work is done to make sure we have the kinds of soil layer information WRF looks for.  The `process_soil_levels(varray)` method should be called on any soil data before writing it.\n",
    "* The `add_to_WPS(varray)` method writes this data to the open intermediate files"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "name": "stderr",
     "output_type": "stream",
     "text": [
      "2020-06-30 12:18:53,044 - WRFInputFormatter - INFO - Loading and subsetting ta\n",
      "2020-06-30 12:18:53,058 - WRFInputFormatter - INFO - Loading variable ta from: ./testdata/sample_reanalysis_upa.nc\n",
      "2020-06-30 12:18:53,151 - WRFInputFormatter - INFO - Data already at requested frequency of 6H\n",
      "2020-06-30 12:18:53,152 - WRFInputFormatter - INFO - Writing ta to WPS files\n",
      "2020-06-30 12:18:53,154 - WRFInputFormatter - INFO -    2000-01-01 00:00:00\n",
      "2020-06-30 12:18:53,190 - WRFInputFormatter - INFO -    2000-01-01 06:00:00\n",
      "2020-06-30 12:18:53,219 - WRFInputFormatter - INFO -    2000-01-01 12:00:00\n",
      "2020-06-30 12:18:53,253 - WRFInputFormatter - INFO -    2000-01-01 18:00:00\n",
      "2020-06-30 12:18:53,290 - WRFInputFormatter - INFO -    2000-01-02 00:00:00\n",
      "2020-06-30 12:18:53,331 - WRFInputFormatter - INFO -    2000-01-02 06:00:00\n",
      "2020-06-30 12:18:53,358 - WRFInputFormatter - INFO -    2000-01-02 12:00:00\n",
      "2020-06-30 12:18:53,402 - WRFInputFormatter - INFO - Loading and subsetting hus\n",
      "2020-06-30 12:18:53,414 - WRFInputFormatter - INFO - Loading variable hus from: ./testdata/sample_reanalysis_upa.nc\n",
      "2020-06-30 12:18:53,487 - WRFInputFormatter - INFO - Data already at requested frequency of 6H\n",
      "2020-06-30 12:18:53,488 - WRFInputFormatter - INFO - Writing hus to WPS files\n",
      "2020-06-30 12:18:53,492 - WRFInputFormatter - INFO -    2000-01-01 00:00:00\n",
      "2020-06-30 12:18:53,561 - WRFInputFormatter - INFO -    2000-01-01 06:00:00\n",
      "2020-06-30 12:18:53,601 - WRFInputFormatter - INFO -    2000-01-01 12:00:00\n",
      "2020-06-30 12:18:53,650 - WRFInputFormatter - INFO -    2000-01-01 18:00:00\n",
      "2020-06-30 12:18:53,695 - WRFInputFormatter - INFO -    2000-01-02 00:00:00\n",
      "2020-06-30 12:18:53,731 - WRFInputFormatter - INFO -    2000-01-02 06:00:00\n",
      "2020-06-30 12:18:53,779 - WRFInputFormatter - INFO -    2000-01-02 12:00:00\n",
      "2020-06-30 12:18:53,832 - WRFInputFormatter - INFO - Loading and subsetting ua\n",
      "2020-06-30 12:18:53,833 - WRFInputFormatter - INFO - Loading variable ua from: ./testdata/sample_reanalysis_upa.nc\n",
      "2020-06-30 12:18:53,907 - WRFInputFormatter - INFO - Data already at requested frequency of 6H\n",
      "2020-06-30 12:18:53,908 - WRFInputFormatter - INFO - Writing ua to WPS files\n",
      "2020-06-30 12:18:53,911 - WRFInputFormatter - INFO -    2000-01-01 00:00:00\n",
      "2020-06-30 12:18:53,956 - WRFInputFormatter - INFO -    2000-01-01 06:00:00\n",
      "2020-06-30 12:18:53,990 - WRFInputFormatter - INFO -    2000-01-01 12:00:00\n",
      "2020-06-30 12:18:54,036 - WRFInputFormatter - INFO -    2000-01-01 18:00:00\n",
      "2020-06-30 12:18:54,063 - WRFInputFormatter - INFO -    2000-01-02 00:00:00\n",
      "2020-06-30 12:18:54,100 - WRFInputFormatter - INFO -    2000-01-02 06:00:00\n",
      "2020-06-30 12:18:54,134 - WRFInputFormatter - INFO -    2000-01-02 12:00:00\n",
      "2020-06-30 12:18:54,180 - WRFInputFormatter - INFO - Loading and subsetting va\n",
      "2020-06-30 12:18:54,181 - WRFInputFormatter - INFO - Loading variable va from: ./testdata/sample_reanalysis_upa.nc\n",
      "2020-06-30 12:18:54,259 - WRFInputFormatter - INFO - Data already at requested frequency of 6H\n",
      "2020-06-30 12:18:54,260 - WRFInputFormatter - INFO - Writing va to WPS files\n",
      "2020-06-30 12:18:54,262 - WRFInputFormatter - INFO -    2000-01-01 00:00:00\n",
      "2020-06-30 12:18:54,322 - WRFInputFormatter - INFO -    2000-01-01 06:00:00\n",
      "2020-06-30 12:18:54,359 - WRFInputFormatter - INFO -    2000-01-01 12:00:00\n",
      "2020-06-30 12:18:54,413 - WRFInputFormatter - INFO -    2000-01-01 18:00:00\n",
      "2020-06-30 12:18:54,440 - WRFInputFormatter - INFO -    2000-01-02 00:00:00\n",
      "2020-06-30 12:18:54,473 - WRFInputFormatter - INFO -    2000-01-02 06:00:00\n",
      "2020-06-30 12:18:54,503 - WRFInputFormatter - INFO -    2000-01-02 12:00:00\n",
      "2020-06-30 12:18:54,541 - WRFInputFormatter - INFO - Loading and subsetting psl\n",
      "2020-06-30 12:18:54,541 - WRFInputFormatter - INFO - Loading variable psl from: ./testdata/sample_reanalysis_sfc.nc\n",
      "2020-06-30 12:18:54,712 - WRFInputFormatter - INFO - Data already at requested frequency of 6H\n",
      "2020-06-30 12:18:54,713 - WRFInputFormatter - INFO - Writing psl to WPS files\n",
      "2020-06-30 12:18:54,720 - WRFInputFormatter - INFO -    2000-01-01 00:00:00\n",
      "2020-06-30 12:18:54,731 - WRFInputFormatter - INFO -    2000-01-01 06:00:00\n",
      "2020-06-30 12:18:54,734 - WRFInputFormatter - INFO -    2000-01-01 12:00:00\n",
      "2020-06-30 12:18:54,739 - WRFInputFormatter - INFO -    2000-01-01 18:00:00\n",
      "2020-06-30 12:18:54,741 - WRFInputFormatter - INFO -    2000-01-02 00:00:00\n",
      "2020-06-30 12:18:54,753 - WRFInputFormatter - INFO -    2000-01-02 06:00:00\n",
      "2020-06-30 12:18:54,760 - WRFInputFormatter - INFO -    2000-01-02 12:00:00\n",
      "2020-06-30 12:18:54,762 - WRFInputFormatter - INFO - Loading and subsetting ps\n",
      "2020-06-30 12:18:54,763 - WRFInputFormatter - INFO - Loading variable ps from: ./testdata/sample_reanalysis_sfc.nc\n",
      "2020-06-30 12:18:54,855 - WRFInputFormatter - INFO - Data already at requested frequency of 6H\n",
      "2020-06-30 12:18:54,860 - WRFInputFormatter - INFO - Writing ps to WPS files\n",
      "2020-06-30 12:18:54,863 - WRFInputFormatter - INFO -    2000-01-01 00:00:00\n",
      "2020-06-30 12:18:54,876 - WRFInputFormatter - INFO -    2000-01-01 06:00:00\n",
      "2020-06-30 12:18:54,880 - WRFInputFormatter - INFO -    2000-01-01 12:00:00\n",
      "2020-06-30 12:18:54,883 - WRFInputFormatter - INFO -    2000-01-01 18:00:00\n",
      "2020-06-30 12:18:54,896 - WRFInputFormatter - INFO -    2000-01-02 00:00:00\n",
      "2020-06-30 12:18:54,900 - WRFInputFormatter - INFO -    2000-01-02 06:00:00\n",
      "2020-06-30 12:18:54,903 - WRFInputFormatter - INFO -    2000-01-02 12:00:00\n",
      "2020-06-30 12:18:54,915 - WRFInputFormatter - INFO - Loading and subsetting tas\n",
      "2020-06-30 12:18:54,919 - WRFInputFormatter - INFO - Loading variable tas from: ./testdata/sample_reanalysis_sfc.nc\n",
      "2020-06-30 12:18:54,996 - WRFInputFormatter - INFO - Data already at requested frequency of 6H\n",
      "2020-06-30 12:18:54,997 - WRFInputFormatter - INFO - Writing tas to WPS files\n",
      "2020-06-30 12:18:55,001 - WRFInputFormatter - INFO -    2000-01-01 00:00:00\n",
      "2020-06-30 12:18:55,008 - WRFInputFormatter - INFO -    2000-01-01 06:00:00\n",
      "2020-06-30 12:18:55,011 - WRFInputFormatter - INFO -    2000-01-01 12:00:00\n",
      "2020-06-30 12:18:55,022 - WRFInputFormatter - INFO -    2000-01-01 18:00:00\n",
      "2020-06-30 12:18:55,030 - WRFInputFormatter - INFO -    2000-01-02 00:00:00\n",
      "2020-06-30 12:18:55,034 - WRFInputFormatter - INFO -    2000-01-02 06:00:00\n",
      "2020-06-30 12:18:55,039 - WRFInputFormatter - INFO -    2000-01-02 12:00:00\n",
      "2020-06-30 12:18:55,041 - WRFInputFormatter - INFO - Loading and subsetting soilw\n",
      "2020-06-30 12:18:55,042 - WRFInputFormatter - INFO - Loading variable soilw from: ./testdata/sample_reanalysis_subsfc.nc\n",
      "2020-06-30 12:18:55,657 - WRFInputFormatter - INFO - Resampling in time to requested frequency\n",
      "2020-06-30 12:18:56,309 - WRFInputFormatter - INFO - Writing soilw to WPS files\n",
      "2020-06-30 12:18:56,313 - WRFInputFormatter - INFO -    2000-01-01 00:00:00\n",
      "2020-06-30 12:18:56,320 - WRFInputFormatter - INFO -    2000-01-01 06:00:00\n",
      "2020-06-30 12:18:56,325 - WRFInputFormatter - INFO -    2000-01-01 12:00:00\n",
      "2020-06-30 12:18:56,333 - WRFInputFormatter - INFO -    2000-01-01 18:00:00\n",
      "2020-06-30 12:18:56,340 - WRFInputFormatter - INFO -    2000-01-02 00:00:00\n",
      "2020-06-30 12:18:56,347 - WRFInputFormatter - INFO -    2000-01-02 06:00:00\n",
      "2020-06-30 12:18:56,355 - WRFInputFormatter - INFO -    2000-01-02 12:00:00\n",
      "2020-06-30 12:18:56,361 - WRFInputFormatter - INFO - Loading and subsetting tsoil\n",
      "2020-06-30 12:18:56,362 - WRFInputFormatter - INFO - Loading variable tsoil from: ./testdata/sample_reanalysis_subsfc.nc\n",
      "2020-06-30 12:18:56,413 - WRFInputFormatter - INFO - Resampling in time to requested frequency\n",
      "2020-06-30 12:18:56,460 - WRFInputFormatter - INFO - Writing tsoil to WPS files\n",
      "2020-06-30 12:18:56,490 - WRFInputFormatter - INFO -    2000-01-01 00:00:00\n",
      "2020-06-30 12:18:56,519 - WRFInputFormatter - INFO -    2000-01-01 06:00:00\n",
      "2020-06-30 12:18:56,554 - WRFInputFormatter - INFO -    2000-01-01 12:00:00\n",
      "2020-06-30 12:18:56,564 - WRFInputFormatter - INFO -    2000-01-01 18:00:00\n",
      "2020-06-30 12:18:56,581 - WRFInputFormatter - INFO -    2000-01-02 00:00:00\n",
      "2020-06-30 12:18:56,593 - WRFInputFormatter - INFO -    2000-01-02 06:00:00\n",
      "2020-06-30 12:18:56,602 - WRFInputFormatter - INFO -    2000-01-02 12:00:00\n"
     ]
    }
   ],
   "source": [
    "for var in list(variable_paths.keys()):\n",
    "    # Load and subset this variable based on \n",
    "    # our spatial and temporal dimensions\n",
    "    # Returns an xarray.DataArray, so this can\n",
    "    # be looked at and plotted\n",
    "    varray = wrfgen.load_and_subset(var)\n",
    "    \n",
    "    # Soil variables need\n",
    "    # additional processing\n",
    "    if var in ['tsoil','soilw']:\n",
    "        varray = wrfgen.process_soil_levels(varray) \n",
    "        \n",
    "    wrfgen.add_to_WPS(varray)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Close out the files\n",
    "This cleanly closes the open WRF intermediate files.  These can then be used for the `metgrid.exe` phase of the WRF processing."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "wrfgen.close_WPS_files()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
